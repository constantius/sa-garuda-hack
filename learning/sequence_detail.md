```plantuml

actor user
entity web

participant "Gateway" as gate

box "On Boarding System" #LightBlue
  participant "Registration\nController" as controller
  participant "Registration\nService" as service
  participant "Registration\nRepo" as repo
endbox

participant "Security System" as sec

activate user

autonumber 1

user -> web+: submit data
web -> gate+: HTTP call
gate -> controller+: submit register data
controller -> controller: check request param
controller -> service+: register(data)

alt if OTP = null | securityID = null
  autonumber 6.1.1
  service -> sec+: send OTP
  return send result
else
  autonumber 6.2.1
  service -> sec+: verify OTP
  return verify status

  autonumber 7
  group do in transaction
    service -> repo+: insert
    return insert result
    service ->>]: send to message broker
  end
end

return register result

controller -> controller: asseble result

return register status
return register status
return render info

deactivate user

```
